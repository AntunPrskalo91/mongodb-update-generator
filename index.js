const argv = require('minimist')(process.argv.slice(2));
const MongoClient = require('mongodb').MongoClient;
var url = "";
var dbName = "";
var colName = "";
var user = "";
var password = "";
var authDb = "";
var ups;
var num;
var interval;
var addMissingFields = false;
var useUnicode = false;

validateArgv(argv);

if (user != "" && password != "" && authDb != "")
{
    var authString = "mongodb://" + user + ":" + password + "@" + url + "/" + authDb;
}
else
{
    var authString = "mongodb://" + url;    
}

MongoClient.connect(authString, function (err, client) 
{
    if (err) throw err;

    db = client.db(dbName);
    col = db.collection(colName);
    interval = 1000/ups;

    var refreshId = setInterval(function()
    {
        if (num == 0)
        {
            clearInterval(refreshId);
            console.log("- Done.");
            process.exit(22);
        }
        else
        {
            sendReq(col); 
            num--;   
        }

    }, interval);
});


function sendReq(col)
{
    col.aggregate([{ $sample: { size: 2 } }]).toArray(function(err, record) 
    {
        if (err) throw err;

        var doc = record[0];
        var _id = record[0]._id;
        var flattenArr = flattenToArr(doc, [], "");
        var set = {};
        var valid = true;

        set["$set"] = {};

        for (var i = 0; i < Math.ceil(Math.random() * 3); i++)
        {
            var field = flattenArr[Math.floor(Math.random() * flattenArr.length)];
            var key = Object.keys(field)[0];

            for (var j = 0; j < Object.keys(set["$set"]).length; j++)
            {
                if (Object.keys(set["$set"])[j].includes(key) || key.includes(Object.keys(set["$set"])[j]))
                {
                    valid = false;
                    break;
                }
            }

            if (valid)
            {
                field = updateField(field);
                set["$set"][key] = field[key];   
            }
        }


        col.update({"_id" : _id}, set, function(err, result)
        {
            if (err)
            {
                console.log("- Fail: id : " + String(_id) + " set: "  + JSON.stringify(set));
            }
            else 
            {
                console.log("- Success: id : " + String(_id) + " set: "  + JSON.stringify(set));
            } 
        });
    }); 
}

function flattenToArr(doc, flattenArr, rootString)
{
    var avoid = ["_id", "_bsontype", "buffer", "sub_type", "position", "$id"]

    for (var prop in doc)
    {
        if (doc.hasOwnProperty(prop) && avoid.indexOf(prop) == -1 && !prop.includes("uuid"))
        {
            if (!Array.isArray(doc) || (Array.isArray(doc) && doc[prop] instanceof Object)) 
            {
                if (rootString == "")
                {
                    var fieldName = prop;
                }
                else
                {
                    var fieldName = rootString + "." + prop;
                }

                var newEntry = {};
                newEntry[fieldName] = doc[prop];            
                flattenArr.push(newEntry);

                if (isNaN(prop) && addMissingFields && doc[prop])
                {
                    var newEntry = {};
                    newEntry[fieldName + "_" + "MISSING"] = doc[prop];            
                    flattenArr.push(newEntry);
                }

                if (doc[prop] instanceof Object)
                {
                    flattenArr = flattenToArr(doc[prop], flattenArr, fieldName);    
                }
            }
        }
    }

    return flattenArr;
}


function updateField(field)
{
    var key = Object.keys(field)[0];
    var value = field[key];

    if (typeof value == "boolean")
    {
        value = !value;
    }
    else if (typeof value == "number")
    {
        value++;
    }
    else if (typeof value == "string")
    {
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var lastChar = value.slice(-1);
        pick = lastChar;

        while (pick == lastChar)
        {
            if (useUnicode)
            {
                pick = getRandomUnicode();
            }
            else
            {
                pick = possible.charAt(Math.floor(Math.random() * possible.length));  
            }
        }
       
        value = value.replace(/.$/, pick);
    }
    else if (typeof value == "object" && value)
    {
        var nestedField = {};
        var randomKey = Object.keys(value)[Math.floor(Math.random() * Object.keys(value).length)]
        nestedField[randomKey] = value[randomKey];

        nestedField = updateField(nestedField);

        value[randomKey] = nestedField[randomKey];
    }

    field[key] = value;

    return field;
}

function getRandomUnicode() {

    hex = "0123456789ABCDEF"
    unicode = "0x"
    
    for (var i = 0; i < 4; i++)
    {
        unicode += hex.charAt(Math.floor(Math.random() * hex.length));    
    }

    return String.fromCharCode(unicode);
}

function validateArgv(argv){
    if (argv.help) {
        console.log(
            `
            PARAM            TYPE             DESCRIPTION
    
            --num             int             number of updates
            --ups             int             number of updates per second
            --url             string          url
            --db              string          database name
            --col             string          collection name 
            --user            string          authentication user (if exists)
            --password        string          authentication password (if exists)
            --authDb          string          authentication database (if exists)
        `
        )
        return process.exit(22);
    }
  
    if (!isNaN(parseInt(argv.num))) {
        num = parseInt(argv.num);
        if (num < 0) {
            console.log("Number for updates for --num can't be negative")
            return process.exit(22);
        }
    } else {
        console.log("Invalid value for number of updates, --num");
        return process.exit(22);
    }
    
    if (!isNaN(parseInt(argv.ups))) {
        ups = parseInt(argv.ups);
        if (ups > 300 || ups < 1) {
            console.log("Invalid value for ups ")
            return process.exit(22);
        }
    } else {
        console.log("Invalid value for mps ");
        return process.exit(22);
    }
    
    if (argv.url) {
        url = argv.url;
    } else {
        console.log("Url --url is not set");
        return process.exit(22);
    }
    
    if (argv.db) {
        dbName = argv.db;
    } else {
        console.log("Database name --db is not set");
        return process.exit(22);
    }
    
    if (argv.col) {
        colName = argv.col;
    } else {
        console.log("Collection name --col is not set");
        return process.exit(22);
    }

    if (argv.user)
    {
        user = argv.user;
    }

    if (argv.password)
    {
        password = argv.password;
    }

    if (argv.authDb)
    {
        authDb = argv.authDb;
    }

    if (argv.addMissingFields) 
    {
        addMissingFields = argv.addMissingFields
    }

    if (argv.useUnicode) 
    {
        useUnicode = argv.useUnicode
    }
}
